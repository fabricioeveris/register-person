package com.everis.registerperson.domain.model;

import java.math.BigDecimal;

public class PersonLegal extends Person {

    private String cnpj;
    private BigDecimal billing;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public BigDecimal getBilling() {
        return billing;
    }

    public void setBilling(BigDecimal billing) {
        this.billing = billing;
    }
}

