package com.everis.registerperson.domain.model;

import java.util.Objects;

public class State {

    private Long id;
    private String name;
    private Countrie countrie;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Countrie getCountrie() {
        return countrie;
    }

    public void setCountrie(Countrie countrie) {
        this.countrie = countrie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        State state = (State) o;
        return Objects.equals(id, state.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "State{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", countrie=" + countrie +
                '}';
    }
}
