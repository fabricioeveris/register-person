package com.everis.registerperson.domain.model;

public class PersonPhysical extends Person {

    private String cpf;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}
