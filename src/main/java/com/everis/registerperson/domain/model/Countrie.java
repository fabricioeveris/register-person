package com.everis.registerperson.domain.model;

import java.util.Objects;

public class Countrie {

    private Long id;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Countrie countrie = (Countrie) o;
        return Objects.equals(id, countrie.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Countrie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
