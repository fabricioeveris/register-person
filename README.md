### Register Person

O objetivo da aplicação é efetuar um cadastro de pessoas, seja ela fisica ou jurídica, respeitando os seguintes requisitos:

#### Negócio
- O cpf é um dado único, ou seja, não pode ser duplicado (mesma regra ao cnpj).
- Pessoa física pode possuir mais de um endereço, mas deve possuir um endereço principal.
- Para pessoa jurídica, deve existir a possibilidade de cadastrar seu faturamento, e este será informado na moeda do pais corrente e convertido em dolar. 
	- Exemplo: Empresa x Brasil; Faturamento: R$ 1.000,00 -> registrado $ 200.00 Dolar.

#### Técnico
- Existe um esboço da ideia (projeto deste repositório) , no que diz respeito a arquitetura e modelagem das classes, sinta-se avontade para modificar caso seja necessário.
- A aplicação deve expor endepoints para: cadastrar, alterar, alterar particalmente, listar e consultar um registro.
- Documente a aplicação
- Procure utilizar melhores práticas de desenvolvimento como SOLID e design patters.
- O cadastro de pessoa, tanto jurídica ou física, deve ser assincrona com uso de mensageria, em específico kafka.
- Para alterar um registro, também deve ser assíncrono com uso de mensageria (outro tópic em kafka).
- Se preocupe com a integridade dos dados, ou seja, não pode ter endereço duplicado por exemplo.
- Faça uma carga inicial dos dados que serão utilizados, no momento do up da app. Exemplo: cidade, estado e pais.
- Para consultar o dolar, utilize a api: https://economia.awesomeapi.com.br/json/all
- Para cada regra de negócio, aplique testes unitários e utilize o mockito com junit5.
- Teste os endpoints com restassured.
- A aplicação estará preparada para escabilidade, ou seja, posso subir mais de uma instancia caso seja necessário (crie um dockerfile da app).